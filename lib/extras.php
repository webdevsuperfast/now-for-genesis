<?php
/**
 * Extras
 *
 * @package 	Now UI for Genesis
 * @since 		1.0
 * @author 		RecommendWP <http://recommendwp.com>
 * @copyright 	Copyright (c) 2017, RecommendWP
 * @license 	http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
 */

// Add class to images
// @link http://stackoverflow.com/a/22078964
add_filter( 'the_content', 'now_image_responsive_class' );
function now_image_responsive_class( $content ) {
   global $post;
   
   $pattern ="/<img(.*?)class=\"(.*?)\"(.*?)>/i";
   $replacement = '<img$1class="$2 img-fluid rounded"$3>';
   $content = preg_replace( $pattern, $replacement, $content );
   
   return $content;
}

//* Add class to blog page template
add_filter( 'body_class', 'page_blog_class' );
function page_blog_class( $classes ) {
    if ( is_page_template( 'page_blog.php' ) ) {
        $classes[] = 'blog';
    }

    return $classes;
}

// Remove Parentheses on Archive/Categories
// @link http://wordpress.stackexchange.com/questions/88545/how-to-remove-the-parentheses-from-the-category-widget
add_filter( 'wp_list_categories', 'now_categories_postcount_filter', 10, 2 );
add_filter( 'get_archives_link', 'now_categories_postcount_filter', 10, 2 );
function now_categories_postcount_filter( $variable ) {
   $variable = str_replace( '(', '<span class="badge badge-default post-count">', $variable );
   $variable = str_replace( ')', '</span>', $variable );
   return $variable;
}

// Filter Responsive Meta Tag
add_filter( 'genesis_viewport_value', 'now_viewport_value', 10, 2 );
function now_viewport_value( $meta ) {
    return 'width=device-width, initial-scale=1, shrink-to-fit=no';
}

add_filter( 'genesis_attr_site-header', 'now_site_header_attr', 10, 2 );
function now_site_header_attr( $attr ) {
    $attr['color-on-scroll'] = '500';

    return $attr;
}

add_filter( 'genesis_attr_page-header-image', 'now_page_header_image_attr', 10, 2 );
function now_page_header_image_attr( $attr ) {
    $attr['data-parallax'] = 'true';
    $attr['style'] = 'background-image: url('.get_stylesheet_directory_uri() . '/images/bg1.jpg)';

    return $attr;
}