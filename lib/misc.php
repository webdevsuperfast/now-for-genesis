<?php
/**
 * Miscellaneous
 *
 * @package 	Now UI for Genesis
 * @since 		1.0
 * @author 		RecommendWP <http://recommendwp.com>
 * @copyright 	Copyright (c) 2017, RecommendWP
 * @license 	http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
 */

// Remove default header
remove_action( 'genesis_header', 'genesis_do_header' );

add_action( 'get_header', 'now_do_theme_settings' );
function now_do_theme_settings() {
    add_action( 'genesis_after_header', 'now_do_page_header' );

    if ( is_page() && !is_front_page() && !is_page_template( 'page_blog.php' ) ) {
        remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
        remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );
        remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

        add_action( 'now_page_header', 'genesis_do_post_title' );
    }

    if ( is_single() ) {
        remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
        remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );
        remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

        remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

        add_action( 'now_page_header', 'genesis_do_post_title' );
        // add_action( 'now_page_header', 'genesis_post_info' );
    }

    remove_action( 'genesis_before_loop', 'genesis_do_blog_template_heading' );
    add_action( 'now_page_header', 'genesis_do_blog_template_heading' );

    remove_action( 'genesis_before_loop', 'genesis_do_posts_page_heading' );
    add_action( 'now_page_header', 'genesis_do_posts_page_heading' );

    remove_action( 'genesis_archive_title_descriptions', 'genesis_do_archive_headings_open', 5, 3 );
    remove_action( 'genesis_archive_title_descriptions', 'genesis_do_archive_headings_close', 15, 3 );
}

function now_do_page_header() {
    do_action( 'now_page_header' );
}

add_action( 'now_page_header', 'now_page_header_markup_open', 5 );
function now_page_header_markup_open() {
    genesis_markup( array(
        'open' => '<div %s>',
        'context' => 'page-header',
    ) );

    genesis_markup( array(
        'open' => '<div %s>',
        'context' => 'page-header-image'
    ) );

    genesis_markup( array(
        'close' => '</div>',
        'context' => 'page-header-image'
    ) );

    genesis_structural_wrap( 'page-header' );
}

add_action( 'now_page_header', 'now_page_header_markup_close', 15 );
function now_page_header_markup_close() {
    genesis_structural_wrap( 'page-header', 'close' );

    genesis_markup( array(
        'close' => '</div>',
        'context' => 'page-header'
    ) );
}

add_filter( 'genesis_attr_page-header', 'now_page_header_attr', 10, 2 );
function now_page_header_attr( $attr ) {
    $classes = array();

    $classes[] = $attr['class'];
    $classes[] = !is_front_page() ? 'page-header-small' : '';

    $attr['class'] = esc_attr( implode( ' ', $classes ) );

    // $attr['data-background-color'] = 'orange';

    return $attr;
}

add_filter( 'genesis_attr_entry-title', 'now_entry_title_attr', 10, 2 );
function now_entry_title_attr( $attr ) {
    if ( ( is_page() || is_single() ) && !is_page_template( 'page_blog.php' ) ) {
        $attr['class'] = $attr['class'] . ' content-center';
    }

    return $attr;
}

add_filter( 'genesis_attr_blog-template-description', 'now_blog_template_attr', 10, 2 );
function now_blog_template_attr( $attr ) {
    $attr['class'] = $attr['class'] . ' content-center';

    return $attr;
}

add_filter( 'genesis_footer_output', 'now_footer_output', 10, 2 );
function now_footer_output( $output ) {
    ob_start();

    if ( ! genesis_nav_menu_supported( 'footer' ) || !has_nav_menu( 'footer' ) )
		return;

	$class = 'menu genesis-nav-menu menu-footer';
	if ( genesis_superfish_enabled() ) {
		$class .= ' js-superfish';
	}

	if ( genesis_a11y( 'headings' ) ) {
		printf( '<h2 class="screen-reader-text">%s</h2>', __( 'Before Footer navigation', 'crunch-analytics' ) );
	}

	genesis_nav_menu( array(
	'theme_location' => 'footer',
	'menu_class'     => $class,
	) );

    genesis_markup( array(
        'open' => '<div %s>',
        'context' => 'copyright'
    ) );

    $creds_text = sprintf( '[footer_copyright before="%s "] &#x000B7; [footer_childtheme_link before="" after=" %s"] [footer_genesis_link url="http://www.studiopress.com/" before=""] &#x000B7; [footer_wordpress_link] &#x000B7; [footer_loginout]', __( 'Copyright', 'genesis' ), __( 'on', 'genesis' ) );

    return $creds_text;

    genesis_markup( array(
        'close' => '</div>',
        'context' => 'copyright'
    ) );

    $output = ob_get_clean();

    return $output;
}
