<?php
/**
 * Navigation
 *
 * @package 	Now UI for Genesis
 * @since 		1.0
 * @author 		RecommendWP <http://recommendwp.com>
 * @copyright 	Copyright (c) 2017, RecommendWP
 * @license 	http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
 */

//* Reposition navigation before site-container
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_header', 'genesis_do_nav' );
// add_action( 'genesis_before', 'genesis_do_nav' );

add_filter( 'wp_nav_menu_args', 'now_menu_args_filter', 10, 2 );
function now_menu_args_filter( $args ) {
    require_once ( B4G_THEME_MODULES . 'navwalker.php' );

    if ( 'primary' === $args['theme_location'] ) {
        $args['container'] = false;
        $args['menu_class'] = 'navbar-nav';
        $args['fallback_cb'] = '__return_false';
        $args['items_wrap'] = '<ul id="%1$s" class="%2$s">%3$s</ul>';
        $args['depth'] = 2;
        $args['walker'] = new b4st_walker_nav_menu();
    }

    return $args;
}

add_filter( 'wp_nav_menu', 'now_nav_markup_filter', 10, 2 );
function now_nav_markup_filter( $html, $args ) {
    if ( 'primary' !== $args->theme_location ) {
        return $html;
    }

    $data_target = 'genesis-nav' . sanitize_html_class( '-' . $args->theme_location );

    $output = '<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#'.$data_target.'" aria-controls="'.$data_target.'" aria-expanded="false" aria-label="Toggle navigation"></button>';
    if ( 'primary' === $args->theme_location ) {
        $output .= apply_filters( 'now_navbar_brand', now_navbar_brand_markup() );
    }
    $output .= '<div class="collapse navbar-collapse justify-content-end" id="'.$data_target.'">';
    $output .= $html;
    // $output .= apply_filters( 'now_navbar_content', now_navbar_content_markup() );
    $output .= '</div>';

    return $output;
}

function now_navbar_brand_markup() {
    $output = '<a class="navbar-brand" id="logo" title="'.esc_attr( get_bloginfo( 'description' ) ).'" href="'.esc_url( home_url( '/' ) ).'">'.get_bloginfo( 'name' ).'</a>';

    return $output;
}

function now_navbar_content_markup() {
    $url = get_home_url();

    $output = <<<EOT
        <form class="form-inline" method="get" action="#{$url}" role="search">
            <input class="form-control mr-sm-2" type="text" placeholder="Search" name="s">
            <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
EOT;
    return $output;
}

//* Filter primary navigation output to match Bootstrap markup
// @link http://wordpress.stackexchange.com/questions/58377/using-a-filter-to-modify-genesis-wp-nav-menu/58394#58394
add_filter( 'genesis_do_nav', 'now_override_do_nav', 10, 3 );
function now_override_do_nav($nav_output, $nav, $args) {
    // return the modified result
    return sprintf( '%1$s', $nav );

}