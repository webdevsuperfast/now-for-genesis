<?php
/**
 * Scripts
 *
 * @package 	Now UI for Genesis
 * @since 		1.0
 * @author 		RecommendWP <http://recommendwp.com>
 * @copyright 	Copyright (c) 2017, RecommendWP
 * @license 	http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
 */

add_action( 'wp_enqueue_scripts', 'now_enqueue_scripts' );
function now_enqueue_scripts() {
    $version = wp_get_theme()->Version;

	wp_enqueue_style( 'app-google-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,700,700i', array(), $version );

	if ( !is_admin() ) {
		wp_enqueue_style( 'app-css', B4G_THEME_CSS . 'app.css' );

		// Disable the superfish script
		wp_deregister_script( 'superfish' );
		wp_deregister_script( 'superfish-args' );

		// Tether JS
		wp_register_script( 'app-tether-js', B4G_THEME_JS . 'tether.min.js', array( 'jquery' ), $version, true );
		wp_enqueue_script( 'app-tether-js' );

		// Bootstrap JS
		wp_register_script( 'app-bootstrap-js', B4G_THEME_JS . 'bootstrap.min.js', array( 'jquery' ), $version, true );
		wp_enqueue_script( 'app-bootstrap-js' );

		wp_register_script( 'app-datepicker-js', B4G_THEME_JS . 'bootstrap-datepicker.min.js', array( 'jquery' ), $version, true );
		wp_enqueue_script( 'app-datepicker-js' );

		wp_register_script( 'app-switch-js', B4G_THEME_JS . 'bootstrap-switch.min.js', array( 'jquery' ), $version, true );
		wp_enqueue_script( 'app-switch-js' );

		wp_register_script( 'app-moment-js', B4G_THEME_JS . 'moment.min.min.js', array( 'jquery' ), $version, true );
		wp_enqueue_script( 'app-moment-js' );

		wp_register_script( 'app-nouislider-js', B4G_THEME_JS . 'nouislider.min.min.js', array( 'jquery' ), $version, true );
		wp_enqueue_script( 'app-nouislider-js' );

		wp_register_script( 'app-now-ui-js', B4G_THEME_JS . 'now-ui-kit.min.js', array( 'jquery' ), $version, true );
		wp_enqueue_script( 'app-now-ui-js' );

		// App JS
		wp_register_script( 'app-js', B4G_THEME_JS . 'app.min.js', array( 'jquery' ), $version, true );
		wp_enqueue_script( 'app-js' );
	}
}