<?php
/**
 * Widget Areas
 *
 * @package 	Now UI for Genesis
 * @since 		1.0
 * @author 		RecommendWP <http://recommendwp.com>
 * @copyright 	Copyright (c) 2017, RecommendWP
 * @license 	http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
 */

// Register Sidebar Function
add_action( 'init', 'now_register_sidebars' );
function now_register_sidebars() {
	// Register Custom Sidebars
	genesis_register_sidebar( array(
		'id' => 'home-featured',
		'name' => __( 'Home Featured', 'b4genesis' ),
		'description' => __( 'This is the home featured area. It uses the jumbotron bootstrap section.', 'bfg' )
	) );
}
