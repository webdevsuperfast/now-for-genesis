<?php
/**
 * Search Form
 *
 * @package 	Now UI for Genesis
 * @since 		1.0
 * @author 		RecommendWP <http://recommendwp.com>
 * @copyright 	Copyright (c) 2017, RecommendWP
 * @license 	http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
 */

add_filter( 'get_search_form', 'now_search_form' );
function now_search_form( $form ) {
    $form = '<form class="form-inline" role="search" method="get" id="searchform" action="' . home_url('/') . '" >
	<input class="form-control mr-2" type="text" value="' . get_search_query() . '" placeholder="' . esc_attr__('Search', 'b4st') . '..." name="s" id="s" />
	<button type="submit" id="searchsubmit" value="'. esc_attr__('Search', 'b4genesis') .'" class="btn btn-primary"><i class="fa fa-search"></i></button>
    </form>';
    return $form;
}
