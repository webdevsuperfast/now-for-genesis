<?php
/**
 * Bootstrap
 *
 * @package 	Now UI for Genesis
 * @since 		1.0
 * @author 		RecommendWP <http://recommendwp.com>
 * @copyright 	Copyright (c) 2017, RecommendWP
 * @license 	http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
 */

// Add row class after wrap
add_filter( 'genesis_structural_wrap-footer-widgets', 'now_filter_structural_wrap', 10, 2 );
function now_filter_structural_wrap( $output, $original_output ) {
    if( 'close' == $original_output ) {
        $output = '</div>' . $output;
    }

    if ( 'open' == $original_output )  {
    	$output = $output . '<div class="row">';
    }
    return $output;
}

// Adds Filters Automatically from Array Keys
// @link https://gist.github.com/bryanwillis/0f22c3ddb0d0b9453ad0
add_action( 'genesis_meta', 'now_add_array_filters_genesis_attr' );
function now_add_array_filters_genesis_attr() {
    $filters = now_merge_genesis_attr_classes();
    
    foreach( array_keys( $filters ) as $context ) {
        $context = "genesis_attr_$context";
        add_filter( $context, 'now_add_markup_sanitize_classes', 10, 2 );
    }
}

// Clean classes output
function now_add_markup_sanitize_classes( $attr, $context ) {
    $classes = array();
    
    if ( has_filter( 'now_clean_classes_output' ) ) {
        $classes = apply_filters( 'now_clean_classes_output', $classes, $context, $attr );
    }
    
    $value = isset( $classes[$context] ) ? $classes[$context] : array();
    
    if ( is_array( $value ) ) {
        $classes_array = $value;
    } else {
        $classes_array = explode( ' ', ( string )$value );
    }

    $classes_array = array_map( 'sanitize_html_class', $classes_array );
    $attr['class'].= ' ' . implode( ' ', $classes_array );
    return $attr;
}

// Default array of classes to add
function now_merge_genesis_attr_classes() {
    // $navclass = get_theme_mod( 'navtype', 'navbar-static-top' );
    $classes = array(
            'content-sidebar-wrap'      => 'row',
            'entry-content'             => 'clearfix',
            'entry-pagination'          => 'clearfix bfg-pagination-numeric',
            'structural-wrap'           => 'container',
            'comment-list'              => 'list-unstyled',
            'ping-list'                 => 'list-unstyled',
            'home-featured'             => 'jumbotron',
            'site-header'               => 'navbar navbar-toggleable-md bg-primary fixed-top navbar-transparent',
            'entry-image'               => 'img-fluid rounded',
            'site-footer'               => 'footer',
            'footer-widgets'            => 'bg-faded text-muted',
            'nav-primary'               => 'collapse navbar-collapse',
            'archive-title'             => 'content-center',
            'site-inner'                => 'section',
            'content'                   => 'col-sm-12'
    );
    
    if ( has_filter( 'now_add_classes' ) ) {
        $classes = apply_filters( 'now_add_classes', $classes );
    }

    return $classes;
}

// Adds classes array to now_add_markup_class() for cleaning
add_filter( 'now_clean_classes_output', 'now_modify_classes_based_on_extras', 10, 3) ;
function now_modify_classes_based_on_extras( $classes, $context, $attr ) {
    $classes = now_merge_genesis_attr_classes( $classes );
    return $classes;
}

// Layout
// Modify bootstrap classes based on genesis_site_layout
add_filter('now_add_classes', 'now_modify_classes_based_on_template', 10, 3);

// Remove unused layouts
function now_layout_options_modify_classes_to_add( $classes_to_add ) {

    $layout = genesis_site_layout();
    
    if ( 'full-width-content' === $layout ) {
        $classes_to_add['content'] = 'col-sm-12';
    }

    return $classes_to_add;
};

function now_modify_classes_based_on_template( $classes_to_add ) {
    $classes_to_add = now_layout_options_modify_classes_to_add( $classes_to_add );

    return $classes_to_add;
}