<?php
/**
 * Home Page
 *
 * @package 	Now UI for Genesis
 * @since 		1.0
 * @author 		RecommendWP <http://recommendwp.com>
 * @copyright 	Copyright (c) 2017, RecommendWP
 * @license 	http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
 */

add_action( 'get_header', 'now_homepage_settings' );
function now_homepage_settings() {
    if ( is_active_sidebar( 'home-featured' ) ) {
        add_action( 'now_page_header', 'now_do_home_featured' );
    }

	//* Remove Title
	if ( get_option( 'show_on_front' ) ) {
		remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
		remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );
		remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
	}
}

function now_do_home_featured() {
	genesis_widget_area( 'home-featured', array(
		'before' => '',
		'after' => ''
	) );
}

add_filter( 'genesis_attr_home-featured', 'now_home_featured_attr', 10, 2 );
function now_home_featured_attr( $attr ) {
	$classes = array();
	$classes[] = $attr['class'];
	$classes[] = 'page-header';
	$classes[] = 'page-header-small';

	$attr['class'] = esc_attr( implode( ' ', $classes ) );

	return $attr;
}

genesis();